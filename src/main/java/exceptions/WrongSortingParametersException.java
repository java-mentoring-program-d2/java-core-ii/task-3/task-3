package exceptions;

public final class WrongSortingParametersException extends VehicleException {
    public WrongSortingParametersException(String message) {
        super(message);
    }
}
