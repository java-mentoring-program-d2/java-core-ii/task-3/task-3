package annotations;

import java.lang.annotation.*;


@Target({ElementType.TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.CONSTRUCTOR, ElementType.PARAMETER,
        ElementType.LOCAL_VARIABLE, ElementType.PACKAGE, ElementType.TYPE_PARAMETER})
@Repeatable(ThisCodeSmellsContainer.class)
public @interface ThisCodeSmells {
    String reviewer();

}

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.CONSTRUCTOR, ElementType.PARAMETER,
        ElementType.LOCAL_VARIABLE, ElementType.PACKAGE, ElementType.TYPE_PARAMETER})
@interface ThisCodeSmellsContainer {
    ThisCodeSmells[] value();
}
